'use strict';
console.clear();

let connect, body = null;
let msg = 'hello, I am expanding my network a little and am open to new offers. Perhaps you have something?';
/*
 * Little config app
 * */
const connectBtn = '.reusable-search__result-container button';
const modalBlock = '.send-invite';
const modalNameRecipient = '.artdeco-modal__content strong';
const msgBlock = '.connect-button-send-invite__custom-message';
const msgBtn = ['BUTTON', 'mr1'];
const actionsBtn = '.artdeco-modal__actionbar button';
const nextPage = 'button.artdeco-pagination__button--next';
const limitOut = 'ip-fuse-limit-alert';
const pause = 3000; // pause in ms
let counter = 0; // day limit ~30

function start(callback) {
  let scroll = {
    heigthMax: +document.documentElement.scrollHeight.toFixed(),
    screen: +document.documentElement.clientHeight.toFixed(),
    positon: +document.documentElement.scrollTop.toFixed(),
    to: function (go) {
      if((this.positon + this.screen) < this.heigthMax) {
        this.positon += 15;
        document.documentElement.scroll(0, this.positon);
      } else {
        clearInterval(go);
        setTimeout(() => callback(), pause);
        return 0;
      }
    }
  };
  document.documentElement.scroll(0, 0);
  let go = setInterval(() => scroll.to(go), 50);
}

function worker() {

  if(counter >=30){
    return console.log('Day limit');
  }

  document.querySelectorAll(connectBtn)
    .forEach((item) => clearButton(item));
  connect = document.querySelector(connectBtn);
  
  if(connect === null) {
    document.documentElement.scroll(0, +document.documentElement.scrollHeight.toFixed());
    
    if(document.querySelector(nextPage)) {
      document.querySelector(nextPage)
        .click();
      console.clear();
      console.log('Next...');
      return setTimeout(() => start(worker), pause);
    }
    return console.log('Finish');
  }
  document.addEventListener('DOMNodeInserted', pressConnect);
  
  connect.click();
}

function pressConnect(event) {

  
  let el = event.target;
  if(el.tagName === 'DIV' && el.classList.contains(limitOut)) {
    console.clear();
    return console.log('Limitited connection stopped');
  }
  if(document.querySelector('.' + limitOut)) {
    console.clear();
    return console.log('Limitited connection stopped');
  }
  
  const actionBar = document.querySelector('.artdeco-modal__actionbar button')
  if(actionBar) {
    counter++;
    insertMessage();
  }
}

function pressApprove(el, callback) {
  connect.remove();
  el.removeEventListener('click', pressApprove);
  setTimeout(() => callback(), pause);
}

// press button approve
function approve(el) {
  el.click();
}

// Add message
function insertMessage() {
  document.removeEventListener('DOMNodeInserted', pressConnect); // delete listener wait popup
  let name = '';
  if(document.querySelector(modalNameRecipient)) {
    name = document.querySelector(modalNameRecipient)
      .textContent.split(' ')[0];
  }
  let popupBtn = document.querySelectorAll(actionsBtn);
  const modal = document.querySelector(modalBlock);
  modal.addEventListener('DOMNodeInserted', (event) => {
    let el = event.target;
    if(el.tagName === 'TEXTAREA') {
      
      setTimeout(()=> popupBtn = document.querySelectorAll(actionsBtn), 300)
      
      document.querySelector(msgBlock)
        .value = `${name} ${msg}`;
      setTimeout(() => {
        var evt = document.createEvent("Events");
        evt.initEvent("change", true, true);
        document.querySelector(msgBlock)
          .dispatchEvent(evt);
        popupBtn[1].addEventListener('click', pressApprove(el, worker));
        approve(popupBtn[1]);
      }, 1000)
      
    }
  });
  
  setTimeout(() => {
    popupBtn[0].click(); // button add note
  }, 1000);
}

// delete buttons already sended connect
function clearButton(el) {
  if(
  el.disabled 
  || el.classList.contains('artdeco-button--muted')
  || el.querySelector('.artdeco-button__text').innerText === 'Повідомлення'
  || el.querySelector('.artdeco-button__text').innerText === 'Спостерігаю'
  || el.querySelector('.artdeco-button__text').innerText === 'Спостерігати'
  || el.querySelector('.artdeco-button__text').innerText === 'Follow'
  || el.querySelector('.artdeco-button__text').innerText === 'Message'

  ) el.remove();
}

start(worker);
